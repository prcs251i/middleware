﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API.Models;

namespace API.Controllers
{
    public class PerformancesController : ApiController
    {
        private CString db = new CString();

        // GET: api/Performances
        public IQueryable<PERFORMANCE> GetPERFORMANCES()
        {
            return db.PERFORMANCES;
        }

        // GET: api/Performances/5
        [ResponseType(typeof(PERFORMANCE))]
        public IHttpActionResult GetPERFORMANCE(decimal id)
        {
            PERFORMANCE pERFORMANCE = db.PERFORMANCES.Find(id);
            if (pERFORMANCE == null)
            {
                return NotFound();
            }

            return Ok(pERFORMANCE);
        }

        // PUT: api/Performances/5
        [ResponseType(typeof(void))]
        [API_Admin]
        public IHttpActionResult PutPERFORMANCE(decimal id, PERFORMANCE pERFORMANCE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pERFORMANCE.ID)
            {
                return BadRequest();
            }

            db.Entry(pERFORMANCE).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PERFORMANCEExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(db.PERFORMANCES.Find(id));
        }

        // POST: api/Performances
        [ResponseType(typeof(PERFORMANCE))]
        [API_Admin]
        public IHttpActionResult PostPERFORMANCE(PERFORMANCE pERFORMANCE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PERFORMANCES.Add(pERFORMANCE);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PERFORMANCEExists(pERFORMANCE.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Ok(db.PERFORMANCES.OrderByDescending(P => P.ID).First());
        }

        // DELETE: api/Performances/5
        [ResponseType(typeof(PERFORMANCE))]
        [API_Admin]
        public IHttpActionResult DeletePERFORMANCE(decimal id)
        {
            PERFORMANCE pERFORMANCE = db.PERFORMANCES.Find(id);
            if (pERFORMANCE == null)
            {
                return NotFound();
            }

            db.PERFORMANCES.Remove(pERFORMANCE);
            db.SaveChanges();

            return Ok(pERFORMANCE);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PERFORMANCEExists(decimal id)
        {
            return db.PERFORMANCES.Count(e => e.ID == id) > 0;
        }
    }
}