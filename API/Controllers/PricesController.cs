﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API.Models;

namespace API.Controllers
{
    public class PricesController : ApiController
    {
        private CString db = new CString();

        // GET: api/Prices
        public IQueryable<PRICE> GetPRICEs()
        {
            return db.PRICEs;
        }

        // GET: api/Prices/5
        [ResponseType(typeof(PRICE))]
        public IHttpActionResult GetPRICE(decimal id)
        {
            PRICE pRICE = db.PRICEs.Find(id);
            if (pRICE == null)
            {
                return NotFound();
            }

            return Ok(pRICE);
        }

        // PUT: api/Prices/5
        [ResponseType(typeof(void))]
        [API_Admin]
        public IHttpActionResult PutPRICE(decimal id, PRICE pRICE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pRICE.ID)
            {
                return BadRequest();
            }

            db.Entry(pRICE).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PRICEExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(db.PRICEs.Find(id));
        }

        // POST: api/Prices
        [ResponseType(typeof(PRICE))]
        [API_Admin]
        public IHttpActionResult PostPRICE(PRICE pRICE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PRICEs.Add(pRICE);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PRICEExists(pRICE.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Ok(db.PRICEs.OrderByDescending(P => P.ID).First());
        }

        // DELETE: api/Prices/5
        [ResponseType(typeof(PRICE))]
        [API_Admin]
        public IHttpActionResult DeletePRICE(decimal id)
        {
            PRICE pRICE = db.PRICEs.Find(id);
            if (pRICE == null)
            {
                return NotFound();
            }

            db.PRICEs.Remove(pRICE);
            db.SaveChanges();

            return Ok(pRICE);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PRICEExists(decimal id)
        {
            return db.PRICEs.Count(e => e.ID == id) > 0;
        }
    }
}