﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using API.Models;
using System.Diagnostics;

namespace API.Controllers
{
    public class VenuesController : ApiController
    {
        private CString db = new CString();

        // GET: api/Venues
        public IQueryable<VENUE> GetVENUEs()
        {
            return db.VENUEs;
        }

        // GET: api/Venues/5
        [ResponseType(typeof(VENUE))]
        public IHttpActionResult GetVENUE(decimal id)
        {
            VENUE vENUE = db.VENUEs.Find(id);
            if (vENUE == null)
            {
                return NotFound();
            }

            return Ok(vENUE);
        }

        // PUT: api/Venues/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutVENUE(decimal id, VENUE vENUE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != vENUE.ID)
            {
                return BadRequest();
            }

            db.Entry(vENUE).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VENUEExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(db.VENUEs.Find(id));
        }

        // POST: api/Venues
        [ResponseType(typeof(VENUE))]
        public IHttpActionResult PostVENUE(VENUE vENUE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.VENUEs.Add(vENUE);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (VENUEExists(vENUE.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }
            return Ok(db.VENUEs.OrderByDescending(V => V.ID).First());
        }

        // DELETE: api/Venues/5
        [ResponseType(typeof(VENUE))]
        public IHttpActionResult DeleteVENUE(decimal id)
        {
            VENUE vENUE = db.VENUEs.Find(id);
            if (vENUE == null)
            {
                return NotFound();
            }

            db.VENUEs.Remove(vENUE);
            db.SaveChanges();

            return Ok(vENUE);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool VENUEExists(decimal id)
        {
            return db.VENUEs.Count(e => e.ID == id) > 0;
        }
    }
}