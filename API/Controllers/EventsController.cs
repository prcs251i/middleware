﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API.Models;

namespace API.Controllers
{
    public class EventsController : ApiController
    {
        private CString db = new CString();

        // GET: api/Events
        public IQueryable<EVENT> GetEVENTs()
        {
            return db.EVENTs;
        }

        // GET: api/Events/5
        [ResponseType(typeof(EVENT))]
        public IHttpActionResult GetEVENT(decimal id)
        {
            EVENT eVENT = db.EVENTs.Find(id);
            if (eVENT == null)
            {
                return NotFound();
            }

            return Ok(eVENT);
        }

        // PUT: api/Events/5
        [ResponseType(typeof(void))]
        [API_Admin]
        public IHttpActionResult PutEVENT(decimal id, EVENT eVENT)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != eVENT.ID)
            {
                return BadRequest();
            }

            db.Entry(eVENT).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EVENTExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(db.EVENTs.Find(id));
        }

        // POST: api/Events
        [ResponseType(typeof(EVENT))]
        [API_Admin]
        public IHttpActionResult PostEVENT(EVENT eVENT)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.EVENTs.Add(eVENT);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (EVENTExists(eVENT.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Ok(db.EVENTs.OrderByDescending(E => E.ID).First());
        }

        // DELETE: api/Events/5
        [ResponseType(typeof(EVENT))]
        [API_Admin]
        public IHttpActionResult DeleteEVENT(decimal id)
        {
            EVENT eVENT = db.EVENTs.Find(id);
            if (eVENT == null)
            {
                return NotFound();
            }

            db.EVENTs.Remove(eVENT);
            db.SaveChanges();

            return Ok(eVENT);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EVENTExists(decimal id)
        {
            return db.EVENTs.Count(e => e.ID == id) > 0;
        }
    }
}