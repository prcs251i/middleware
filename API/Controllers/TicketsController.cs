﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API.Models;

namespace API.Controllers
{
    public class TicketsController : ApiController
    {
        private CString db = new CString();

        // GET: api/Tickets
        [API_Admin]
        public IQueryable<TICKET> GetTICKETs()
        {
            return db.TICKETs;
        }

        // GET: api/Tickets/5
        [ResponseType(typeof(TICKET))]
        [API_Admin]
        public IHttpActionResult GetTICKET(decimal id)
        {
            TICKET tICKET = db.TICKETs.Find(id);
            if (tICKET == null)
            {
                return NotFound();
            }

            return Ok(tICKET);
        }

        // PUT: api/Tickets/5
        [ResponseType(typeof(void))]
        [API_Admin]
        public IHttpActionResult PutTICKET(decimal id, TICKET tICKET)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tICKET.ID)
            {
                return BadRequest();
            }

            db.Entry(tICKET).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TICKETExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(db.TICKETs.Find(id));
        }

        // POST: api/Tickets
        [ResponseType(typeof(TICKET))]
        [API_Admin]
        public IHttpActionResult PostTICKET(TICKET tICKET)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TICKETs.Add(tICKET);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (TICKETExists(tICKET.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Ok(db.TICKETs.OrderByDescending(T => T.ID).First());
        }

        // DELETE: api/Tickets/5
        [ResponseType(typeof(TICKET))]
        [API_Admin]
        public IHttpActionResult DeleteTICKET(decimal id)
        {
            TICKET tICKET = db.TICKETs.Find(id);
            if (tICKET == null)
            {
                return NotFound();
            }

            db.TICKETs.Remove(tICKET);
            db.SaveChanges();

            return Ok(tICKET);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TICKETExists(decimal id)
        {
            return db.TICKETs.Count(e => e.ID == id) > 0;
        }
    }
}