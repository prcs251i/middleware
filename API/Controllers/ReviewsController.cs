﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API.Models;

namespace API.Controllers
{
    public class ReviewsController : ApiController
    {
        private CString db = new CString();

        // GET: api/Reviews
        public IQueryable<REVIEW> GetREVIEWs()
        {
            return db.REVIEWs;
        }

        // GET: api/Reviews/5
        [ResponseType(typeof(REVIEW))]
        public IHttpActionResult GetREVIEW(decimal id)
        {
            REVIEW rEVIEW = db.REVIEWs.Find(id);
            if (rEVIEW == null)
            {
                return NotFound();
            }

            return Ok(rEVIEW);
        }

        // PUT: api/Reviews/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutREVIEW(decimal id, REVIEW rEVIEW)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != rEVIEW.ID)
            {
                return BadRequest();
            }

            db.Entry(rEVIEW).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!REVIEWExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(db.REVIEWs.Find(id));
        }

        // POST: api/Reviews
        [ResponseType(typeof(REVIEW))]
        public IHttpActionResult PostREVIEW(REVIEW rEVIEW)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.REVIEWs.Add(rEVIEW);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (REVIEWExists(rEVIEW.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Ok(db.REVIEWs.OrderByDescending(R => R.ID).First());
        }

        // DELETE: api/Reviews/5
        [ResponseType(typeof(REVIEW))]
        public IHttpActionResult DeleteREVIEW(decimal id)
        {
            REVIEW rEVIEW = db.REVIEWs.Find(id);
            if (rEVIEW == null)
            {
                return NotFound();
            }

            db.REVIEWs.Remove(rEVIEW);
            db.SaveChanges();

            return Ok(rEVIEW);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool REVIEWExists(decimal id)
        {
            return db.REVIEWs.Count(e => e.ID == id) > 0;
        }
    }
}