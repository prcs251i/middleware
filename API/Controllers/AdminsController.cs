﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API.Models;

namespace API.Controllers
{
    public class AdminsController : ApiController
    {
        private CString db = new CString();

        // GET: api/Admins
        [API_Admin]
        public IQueryable<ADMIN> GetADMINs()
        {
            return db.ADMINs;
        }

        // GET: api/Admins/5
        [ResponseType(typeof(ADMIN))]
        [API_Admin]
        public IHttpActionResult GetADMIN(decimal id)
        {
            ADMIN aDMIN = db.ADMINs.Find(id);
            if (aDMIN == null)
            {
                return NotFound();
            }

            return Ok(aDMIN);
        }

        // PUT: api/Admins/5
        [ResponseType(typeof(void))]
        [API_Admin]
        public IHttpActionResult PutADMIN(decimal id, ADMIN aDMIN)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != aDMIN.ID)
            {
                return BadRequest();
            }
            if (!aDMIN.PASSWORD_HASH.EndsWith("=")) aDMIN.PASSWORD_HASH = Authenticator.hash(aDMIN.EMAIL, aDMIN.PASSWORD_HASH);
            db.Entry(aDMIN).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ADMINExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(db.ADMINs.Find(id));
        }

        // POST: api/Admins
        [ResponseType(typeof(ADMIN))]
        [API_Admin]
        public IHttpActionResult PostADMIN(ADMIN aDMIN)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            aDMIN.PASSWORD_HASH = Authenticator.hash(aDMIN.EMAIL, aDMIN.PASSWORD_HASH);
            db.ADMINs.Add(aDMIN);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ADMINExists(aDMIN.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Ok(db.ADMINs.OrderByDescending(A => A.ID).First());
        }

        // DELETE: api/Admins/5 
        [ResponseType(typeof(ADMIN))]
        [API_Admin]
        public IHttpActionResult DeleteADMIN(decimal id)
        {
            ADMIN aDMIN = db.ADMINs.Find(id);
            if (aDMIN == null)
            {
                return NotFound();
            }

            db.ADMINs.Remove(aDMIN);
            db.SaveChanges();

            return Ok(aDMIN);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ADMINExists(decimal id)
        {
            return db.ADMINs.Count(e => e.ID == id) > 0;
        }
    }
}