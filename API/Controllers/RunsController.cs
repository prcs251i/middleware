﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API.Models;

namespace API.Controllers
{
    public class RunsController : ApiController
    {
        private CString db = new CString();

        // GET: api/Runs
        public IQueryable<RUN> GetRUNs()
        {
            return db.RUNs;
        }

        // GET: api/Runs/5
        [ResponseType(typeof(RUN))]
        public IHttpActionResult GetRUN(decimal id)
        {
            RUN rUN = db.RUNs.Find(id);
            if (rUN == null)
            {
                return NotFound();
            }

            return Ok(rUN);
        }

        // PUT: api/Runs/5
        [ResponseType(typeof(void))]
        [API_Admin]
        public IHttpActionResult PutRUN(decimal id, RUN rUN)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != rUN.ID)
            {
                return BadRequest();
            }

            db.Entry(rUN).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RUNExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(db.RUNs.Find(id));
        }

        // POST: api/Runs
        [ResponseType(typeof(RUN))]
        [API_Admin]
        public IHttpActionResult PostRUN(RUN rUN)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.RUNs.Add(rUN);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (RUNExists(rUN.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Ok(db.RUNs.OrderByDescending(R => R.ID).First());
        }

        // DELETE: api/Runs/5
        [ResponseType(typeof(RUN))]
        [API_Admin]
        public IHttpActionResult DeleteRUN(decimal id)
        {
            RUN rUN = db.RUNs.Find(id);
            if (rUN == null)
            {
                return NotFound();
            }

            db.RUNs.Remove(rUN);
            db.SaveChanges();

            return Ok(rUN);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RUNExists(decimal id)
        {
            return db.RUNs.Count(e => e.ID == id) > 0;
        }
    }
}