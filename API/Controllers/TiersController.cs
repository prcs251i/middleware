﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API.Models;

namespace API.Controllers
{
    public class TiersController : ApiController
    {
        private CString db = new CString();

        // GET: api/Tiers
        public IQueryable<TIER> GetTIERs()
        {
            return db.TIERs;
        }

        // GET: api/Tiers/57
        [ResponseType(typeof(TIER))]
        public IHttpActionResult GetTIER(decimal id)
        {
            TIER tIER = db.TIERs.Find(id);
            if (tIER == null)
            {
                return NotFound();
            }

            return Ok(tIER);
        }

        // PUT: api/Tiers/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTIER(decimal id, TIER tIER)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tIER.ID)
            {
                return BadRequest();
            }

            db.Entry(tIER).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TIERExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(db.TIERs.Find(id));
        }

        // POST: api/Tiers
        [ResponseType(typeof(TIER))]
        public IHttpActionResult PostTIER(TIER tIER)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TIERs.Add(tIER);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (TIERExists(tIER.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Ok(db.TIERs.OrderByDescending(T => T.ID).First());
        }

        // DELETE: api/Tiers/5
        [ResponseType(typeof(TIER))]
        public IHttpActionResult DeleteTIER(decimal id)
        {
            TIER tIER = db.TIERs.Find(id);
            if (tIER == null)
            {
                return NotFound();
            }

            db.TIERs.Remove(tIER);
            db.SaveChanges();

            return Ok(tIER);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TIERExists(decimal id)
        {
            return db.TIERs.Count(e => e.ID == id) > 0;
        }
    }
}