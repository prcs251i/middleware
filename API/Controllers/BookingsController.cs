﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web;
using API.Models;

namespace API.Controllers
{

    public class BookingsController : ApiController
    {
        private CString db = new CString();
        public class Seat
        {
            public int row { get; set; }
            public int col { get; set; }
            public Seat(int row,int col)
            {
                this.row = row;
                this.col = col;
            }
        }
        public class BookingRequest
        {
            public int quantity { get; set; }
            public Decimal personID { get; set; }
            public Decimal tierID { get; set; }
            public Decimal runID { get; set; }
            public Decimal totalCost { get; set; }
        }
        public class GuestBookingRequest
        {
            public int quantity { get; set; }
            public Decimal tierID { get; set; }
            public Decimal runID { get; set; }
            public Decimal totalCost { get; set; }
            public String email { get; set; }
            public String forename { get; set; }
            public String surname { get; set; }
        }
        [NonAction]
        public Seat getSeat(TICKET T)
        {
            if (T == null) return new Seat(1, 1);
            TIER tier = T.TIER;
            Int32 row = Int32.Parse(T.SEAT_ROW);
            Int32 col = Int32.Parse(T.SEAT_NUM) + 1;
            if(col >= tier.SEAT_COLUMNS)
            {
                row = row + 1;
                col = 1;
            }
            return new Seat(row, col);
        }

        [NonAction]
        public int getTicketCount(TIER t, RUN r)
        {
            return db.TICKETs
                .Where(T => T.TIER_ID == t.ID && T.RUN_ID == r.ID)
                .Count();
        }
        
        [Route("api/bookings/createBooking")]
        [ResponseType(typeof(BOOKING))]
        public IHttpActionResult createTicketsAndBooking(BookingRequest req)
        {
            TIER t = db.TIERs.Find(req.tierID);
            if (t == null) return BadRequest("Tier ID provided does not exist in the database.");
            CUSTOMER c = db.CUSTOMERs.Find(req.personID);
            if(c == null) return BadRequest("Customer ID provided does not exist in the database.");
            RUN r = db.RUNs.Find(req.runID);
            int count = getTicketCount(t, r);
            if (count >= t.CAPACITY) return Conflict();
            if (r == null) return BadRequest("Run ID provided does not exist in the database.");
            BOOKING newBooking = new BOOKING();
            newBooking.ORDER_DATE = new DateTime();
            newBooking.QUANTITY = req.quantity;
            newBooking.RUN_ID = req.runID;
            newBooking.ID = 0;
            newBooking.TOTAL_COST = req.totalCost;
            newBooking.PERSON_ID = req.personID;
            db.BOOKINGs.Add(newBooking);
            db.SaveChanges();
            newBooking = db.BOOKINGs.OrderByDescending(B => B.ID).First();
            Decimal bookingId = newBooking.ID;
            TICKET prevTicket = db.TICKETs
                .Where(T => T.TIER_ID == t.ID && T.RUN_ID == r.ID)
                .OrderByDescending(T => T.ID).FirstOrDefault();
            for(int i = 0; i < req.quantity; i++)
            {
                TICKET newTicket = new TICKET();
                newTicket.BOOKING_ID = bookingId;
                newTicket.ID = 0;
                newTicket.TIER_ID = req.tierID;
                Seat s = getSeat(prevTicket);
                newTicket.SEAT_NUM = s.col.ToString();
                newTicket.SEAT_ROW = s.row.ToString();
                newTicket.GUEST_BOOKING_ID = null;
                newTicket.RUN_ID = r.ID;
                prevTicket = newTicket;
                db.TICKETs.Add(newTicket);
            }
            try
            {
                db.SaveChanges();
            }
            catch (Exception)   
            {
                BOOKING res = db.BOOKINGs.OrderByDescending(B => B.ID).First();
                return Ok(res);
            }
            BOOKING result = db.BOOKINGs.OrderByDescending(B => B.ID).First();
            return Ok(result);
        }

        [Route("api/bookings/createGuestBooking")]
        [ResponseType(typeof(BOOKING))]
        public IHttpActionResult createGuestTicketsAndBooking(GuestBookingRequest req)
        {
            TIER t = db.TIERs.Find(req.tierID);
            if (t == null) return BadRequest("Tier ID provided does not exist in the database.");
            RUN r = db.RUNs.Find(req.runID);
            if (r == null) return BadRequest("Run ID provided does not exist in the database.");
            int count = getTicketCount(t, r);
            if (count >= t.CAPACITY) return Conflict();
            GUEST_BOOKING newBooking = new GUEST_BOOKING();
            newBooking.ID = 0;
            newBooking.ORDER_DATE = DateTime.Now;
            newBooking.QUANTITY = req.quantity;
            newBooking.EMAIL = req.email;
            newBooking.RUN_ID = req.runID;
            newBooking.TOTAL_COST = req.totalCost;
            newBooking.FORENAME = req.forename;
            newBooking.SURNAME = req.surname;
            db.GUEST_BOOKING.Add(newBooking);
            db.SaveChanges();
            newBooking = db.GUEST_BOOKING.OrderByDescending(B => B.ID).First();
            Decimal bookingId = newBooking.ID;
            TICKET prevTicket = db.TICKETs
                .Where(T => T.TIER_ID == t.ID && T.RUN_ID == r.ID)
                .OrderByDescending(T => T.ID).FirstOrDefault();
            for (int i = 0; i < req.quantity; i++)
            {
                TICKET newTicket = new TICKET();
                newTicket.BOOKING_ID = bookingId;
                newTicket.ID = 0;
                newTicket.TIER_ID = req.tierID;
                Seat s = getSeat(prevTicket);
                newTicket.SEAT_NUM = s.col.ToString();
                newTicket.SEAT_ROW = s.row.ToString();
                newTicket.BOOKING_ID = null;
                newTicket.GUEST_BOOKING_ID = bookingId;
                prevTicket = newTicket;
                newTicket.RUN_ID = r.ID; newTicket.RUN_ID = r.ID;
                db.TICKETs.Add(newTicket);
            }
            try
            {
                db.SaveChanges();
            }
            catch (Exception)
            {
                GUEST_BOOKING res = db.GUEST_BOOKING.OrderByDescending(B => B.ID).First();
                return Ok(res);
            }
            GUEST_BOOKING result = db.GUEST_BOOKING.OrderByDescending(B => B.ID).First();
            return Ok(result);
        }

        [HttpGet]
        [Route("api/bookings/myBookings/{id}")]
        public IQueryable<BOOKING> myBookings(int id)
        {
            return db.BOOKINGs.Where(B => B.PERSON_ID == id);
        }

        // GET: api/Bookings
        [API_Admin]
        public IQueryable<BOOKING> GetBOOKINGs()
        {
            return db.BOOKINGs;
        }

        // GET: api/Bookings/5
        [ResponseType(typeof(BOOKING))]
        public IHttpActionResult GetBOOKING(decimal id)
        {
            BOOKING bOOKING = db.BOOKINGs.Find(id);
            if (bOOKING == null)
            {
                return NotFound();
            }

            return Ok(bOOKING);
        }

        // PUT: api/Bookings/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutBOOKING(decimal id, BOOKING bOOKING)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != bOOKING.ID)
            {
                return BadRequest();
            }

            db.Entry(bOOKING).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BOOKINGExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(db.BOOKINGs.Find(id));
        }

        // DELETE: api/Bookings/5
        [ResponseType(typeof(BOOKING))]
        [API_Admin]
        public IHttpActionResult DeleteBOOKING(decimal id)
        {
            BOOKING bOOKING = db.BOOKINGs.Find(id);
            if (bOOKING == null)
            {
                return NotFound();
            }

            db.BOOKINGs.Remove(bOOKING);
            db.SaveChanges();

            return Ok(bOOKING);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BOOKINGExists(decimal id)
        {
            return db.BOOKINGs.Count(e => e.ID == id) > 0;
        }
    }
}