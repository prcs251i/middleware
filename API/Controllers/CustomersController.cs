﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API.Models;
using System.Web;
using System.Text;

namespace API.Controllers
{
    public class CustomersController : ApiController
    {
        private CString db = new CString();

        // GET: api/Customers
        [API_Admin]
        public IQueryable<CUSTOMER> GetCUSTOMERs()
        {
            return db.CUSTOMERs;
        }

        // GET: api/Customers/5
        [ResponseType(typeof(CUSTOMER))]
        public IHttpActionResult GetCUSTOMER(decimal id)
        {
            CUSTOMER cUSTOMER = db.CUSTOMERs.Find(id);
            if (cUSTOMER == null)
            {
                return NotFound();
            }

            return Ok(cUSTOMER);
        }

        // PUT: api/Customers/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCUSTOMER(decimal id, CUSTOMER cUSTOMER)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cUSTOMER.ID)
            {
                return BadRequest();
            }
            if(!cUSTOMER.PASSWORD_HASH.EndsWith("=")) cUSTOMER.PASSWORD_HASH = Authenticator.hash(cUSTOMER.EMAIL, cUSTOMER.PASSWORD_HASH);
            db.Entry(cUSTOMER).State = EntityState.Modified;
            

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CUSTOMERExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(db.CUSTOMERs.Find(id));
        }

        // POST: api/Customers
        [ResponseType(typeof(CUSTOMER))]
        public IHttpActionResult PostCUSTOMER(CUSTOMER cUSTOMER)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            cUSTOMER.PASSWORD_HASH = Authenticator.hash(cUSTOMER.EMAIL, cUSTOMER.PASSWORD_HASH);

            db.CUSTOMERs.Add(cUSTOMER);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (CUSTOMERExists(cUSTOMER.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Ok(db.CUSTOMERs.OrderByDescending(C => C.ID).First());
        }

        // DELETE: api/Customers/5
        [ResponseType(typeof(CUSTOMER))]
        [API_Admin]
        public IHttpActionResult DeleteCUSTOMER(decimal id)
        {
            CUSTOMER cUSTOMER = db.CUSTOMERs.Find(id);
            if (cUSTOMER == null)
            {
                return NotFound();
            }

            db.CUSTOMERs.Remove(cUSTOMER);
            db.SaveChanges();

            return Ok(cUSTOMER);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CUSTOMERExists(decimal id)
        {
            return db.CUSTOMERs.Count(e => e.ID == id) > 0;
        }
    }
}