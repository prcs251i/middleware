﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API.Models;

namespace API.Controllers
{
    public class Guest_BookingsController : ApiController
    {
        private CString db = new CString();

        // GET: api/Guest_Bookings
        [API_Admin]
        public IQueryable<GUEST_BOOKING> GetGUEST_BOOKING()
        {
            return db.GUEST_BOOKING;
        }

        // GET: api/Guest_Bookings/5
        [ResponseType(typeof(GUEST_BOOKING))]
        [API_Admin]
        public IHttpActionResult GetGUEST_BOOKING(decimal id)
        {
            GUEST_BOOKING gUEST_BOOKING = db.GUEST_BOOKING.Find(id);
            if (gUEST_BOOKING == null)
            {
                return NotFound();
            }

            return Ok(gUEST_BOOKING);
        }

        // PUT: api/Guest_Bookings/5
        [ResponseType(typeof(void))]
        [API_Admin]
        public IHttpActionResult PutGUEST_BOOKING(decimal id, GUEST_BOOKING gUEST_BOOKING)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != gUEST_BOOKING.ID)
            {
                return BadRequest();
            }

            db.Entry(gUEST_BOOKING).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GUEST_BOOKINGExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(db.GUEST_BOOKING.Find(id));
        }

        // DELETE: api/Guest_Bookings/5
        [ResponseType(typeof(GUEST_BOOKING))]
        [API_Admin]
        public IHttpActionResult DeleteGUEST_BOOKING(decimal id)
        {
            GUEST_BOOKING gUEST_BOOKING = db.GUEST_BOOKING.Find(id);
            if (gUEST_BOOKING == null)
            {
                return NotFound();
            }

            db.GUEST_BOOKING.Remove(gUEST_BOOKING);
            db.SaveChanges();

            return Ok(gUEST_BOOKING);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool GUEST_BOOKINGExists(decimal id)
        {
            return db.GUEST_BOOKING.Count(e => e.ID == id) > 0;
        }
    }
}