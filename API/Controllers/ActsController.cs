﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API.Models;

namespace API.Controllers
{
    public class ActsController : ApiController
    {
        private CString db = new CString();

        // GET: api/Acts
        public IQueryable<ACT> GetACTS()
        {
            return db.ACTS;
        }

        // GET: api/Acts/5
        [ResponseType(typeof(ACT))]
        public IHttpActionResult GetACT(decimal id)
        {
            ACT aCT = db.ACTS.Find(id);
            if (aCT == null)
            {
                return NotFound();
            }

            return Ok(aCT);
        }

        // PUT: api/Acts/5
        [ResponseType(typeof(void))]
        [API_Admin]
        public IHttpActionResult PutACT(decimal id, ACT aCT)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != aCT.ID)
            {
                return BadRequest();
            }

            db.Entry(aCT).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ACTExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(db.ACTS.Find(id));
        }

        // POST: api/Acts
        [ResponseType(typeof(ACT))]
        [API_Admin]
        public IHttpActionResult PostACT(ACT aCT)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ACTS.Add(aCT);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ACTExists(aCT.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Ok(db.ACTS.OrderByDescending(A => A.ID).First());
        }

        // DELETE: api/Acts/5
        [ResponseType(typeof(ACT))]
        [API_Admin]
        public IHttpActionResult DeleteACT(decimal id)
        {
            ACT aCT = db.ACTS.Find(id);
            if (aCT == null)
            {
                return NotFound();
            }

            db.ACTS.Remove(aCT);
            db.SaveChanges();

            return Ok(aCT);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ACTExists(decimal id)
        {
            return db.ACTS.Count(e => e.ID == id) > 0;
        }
    }
}