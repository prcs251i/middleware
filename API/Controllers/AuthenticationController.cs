﻿using API.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Web.Http;
using System.Web;

namespace API.Controllers
{
    interface AuthenticationRequest
    {
        bool validate();
    }
    public abstract class AuthenticationRequestBase : AuthenticationRequest
    {
        public decimal id { get; set; }
        public String email { get; set; }
        public String password { get; set; }
        public CString db = new CString();
        public abstract bool validate();
    }
    public class AdminAuthenticationRequest : AuthenticationRequestBase
    {
        public override Boolean validate()
        {
            ADMIN userInstance = db.ADMINs.Where(A => A.EMAIL == this.email).FirstOrDefault();
            if (userInstance == null) return false;
            id = userInstance.ID;
            return Authenticator.validate(email, password, userInstance.PASSWORD_HASH);
        }
    }
    public class CustomerAuthenticationRequest : AuthenticationRequestBase
    {
        public override Boolean validate()
        {
            CUSTOMER userInstance = db.CUSTOMERs.Where(C => C.EMAIL == this.email).FirstOrDefault();
            if (userInstance == null) return false;
            id = userInstance.ID;
            return Authenticator.validate(email, password, userInstance.PASSWORD_HASH);
        }
    }

    public class AuthenticationController : ApiController
    {
        CString db = new CString();
        [HttpPost]
        [Route("api/auth/adminAuthRequest")]
        public HttpResponseMessage checkAdminLogin(AdminAuthenticationRequest req)
        {
            if (req.validate())
            {
                HttpContext.Current.Session["authenticatedAdmin"] = true;
                return Request.CreateResponse(HttpStatusCode.OK, (ADMIN) db.ADMINs.Find(req.id));
            }
            else
            {
                HttpError err = new HttpError("Invalid username / password.");
                return Request.CreateErrorResponse(HttpStatusCode.Forbidden, err);
            }
            
        }
        [HttpPost]
        [Route("api/auth/customerAuthRequest")]
        public HttpResponseMessage checkCustomerLogin(CustomerAuthenticationRequest req)
        {
            if (req.validate())
            {
                HttpContext.Current.Session["authenticatedCustomer"] = true;
                HttpContext.Current.Session["customerID"] = req.id;
                return Request.CreateResponse(HttpStatusCode.OK, (CUSTOMER) db.CUSTOMERs.Find(req.id));
            }
            else
            {
                HttpError err = new HttpError("Invalid username / password.");
                return Request.CreateErrorResponse(HttpStatusCode.Forbidden, err);
            }

        }
    }
}

