﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using API.Models;

namespace API.Controllers
{
    public class Event_TypesController : ApiController
    {
        private CString db = new CString();

        // GET: api/Event_Types
        public IQueryable<EVENT_TYPE> GetEVENT_TYPE()
        {
            return db.EVENT_TYPE;
        }

        // GET: api/Event_Types/5
        [ResponseType(typeof(EVENT_TYPE))]
        [API_Admin]
        public IHttpActionResult GetEVENT_TYPE(decimal id)
        {
            EVENT_TYPE eVENT_TYPE = db.EVENT_TYPE.Find(id);
            if (eVENT_TYPE == null)
            {
                return NotFound();
            }

            return Ok(eVENT_TYPE);
        }

        // PUT: api/Event_Types/5
        [ResponseType(typeof(void))]
        [API_Admin]
        public IHttpActionResult PutEVENT_TYPE(decimal id, EVENT_TYPE eVENT_TYPE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != eVENT_TYPE.ID)
            {
                return BadRequest();
            }

            db.Entry(eVENT_TYPE).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EVENT_TYPEExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(db.EVENT_TYPE.Find(id));
        }

        // POST: api/Event_Types
        [ResponseType(typeof(EVENT_TYPE))]
        [API_Admin]
        public IHttpActionResult PostEVENT_TYPE(EVENT_TYPE eVENT_TYPE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.EVENT_TYPE.Add(eVENT_TYPE);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (EVENT_TYPEExists(eVENT_TYPE.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Ok(db.EVENT_TYPE.OrderByDescending(E => E.ID).First());
        }

        // DELETE: api/Event_Types/5
        [ResponseType(typeof(EVENT_TYPE))]
        [API_Admin]
        public IHttpActionResult DeleteEVENT_TYPE(decimal id)
        {
            EVENT_TYPE eVENT_TYPE = db.EVENT_TYPE.Find(id);
            if (eVENT_TYPE == null)
            {
                return NotFound();
            }

            db.EVENT_TYPE.Remove(eVENT_TYPE);
            db.SaveChanges();

            return Ok(eVENT_TYPE);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EVENT_TYPEExists(decimal id)
        {
            return db.EVENT_TYPE.Count(e => e.ID == id) > 0;
        }
    }
}