﻿using System;
using System.Web;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Web.Mvc;
using System.Web.Http.Controllers;
namespace API
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = false)]
    public class API_AdminAttribute : System.Web.Http.AuthorizeAttribute
    {
        protected override bool IsAuthorized(HttpActionContext context)
        {
            try {
                if (HttpContext.Current.Session != null && (bool)HttpContext.Current.Session["authenticatedAdmin"] == true) return true;
                else return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
    public class API_CustomerAttribute : System.Web.Http.AuthorizeAttribute
    {
        protected override bool IsAuthorized(HttpActionContext context)
        {
            try
            {
                if (HttpContext.Current.Session != null && (bool)HttpContext.Current.Session["authenticatedAdmin"] == true) return true;
                else if (HttpContext.Current.Session != null && (bool)HttpContext.Current.Session["authenticatedCustomer"] == true) return true;
                else return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
    public class Authenticator
    {
        static String salt = "ajdgjkhdsg/!+";
        static SHA256Cng crypt = new SHA256Cng();
        public static String hash(String username, String password)
        {
            String salted = salt + "/" + username + "/\\" + password + "^/";
            byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(salted);
            byte[] hashArray = crypt.ComputeHash(byteArray);
            return Convert.ToBase64String(hashArray);
             
        }
        public static Boolean validate(String username, String password, String hash){
            return (hash == Authenticator.hash(username, password));
        }

    }
}