//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace API.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TIER
    {
        public decimal ID { get; set; }
        public decimal VENUE_ID { get; set; }
        public string TIER_NAME { get; set; }
        public string SEATED { get; set; }
        public decimal CAPACITY { get; set; }
        public Nullable<decimal> SEAT_ROWS { get; set; }
        public Nullable<decimal> SEAT_COLUMNS { get; set; }
    }
}
